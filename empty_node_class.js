/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

(function ($, Drupal, window, document, undefined) {
  Drupal.behaviors.emptyNodeClass = {
    attach: function (context, options) {
      $('.node', context).each(function () {
        var node = $(this);

        if (!/\S/.test(node.html())) {
          node.addClass('empty');
        }
      });
    }
  };
})(jQuery, Drupal, this, this.document);
